const Twit = require('twit');
const Eris = require('eris');
const Wit = require('node-wit').Wit;

var config = require('./config');

var NO_ARGUMENT_ERROR_STRING = "This command requires an argument!";

// Setting up twit, eris, and wit
const bot = new Eris.CommandClient(config.discord, {}, {
    prefix: "t!"
});

const T = new Twit(config.twitter);

const wit_client = new Wit({
    accessToken: config.wit
});

// Done

bot.on("ready", () => {
    console.log("Discord ready!");
});


// Discord commands

let channel_id;

bot.registerCommand("tweet", tweet_command);

bot.registerCommand("purge", remove_tweets_command, {
    description: "Removes x amount of tweets",
    fullDescription: "Removes a certain amount of tweets, spesificed by paramater",
    hidden: true,
    argsRequired: true,
    permissionMessage: "Sorry pleb, you're not mighty enough for this command.",

    requirements: {
	permissions: { administrator: true }
    }
});

bot.registerCommand("join", join_voice_channel_command);

bot.registerCommand("leave", leave_voice_channel_command);

function leave_voice_channel_command(msg, args) {
    bot.leaveVoiceChannel(msg.member.voiceState.channelID);
}

function join_voice_channel_command(msg, args) {
  if (args.length > 0) {
		return "This command does not take arguments";
  }

  if (!msg.channel.guild) {
		return "Command only possible on servers, sorry cunt";
  }
  
  if (!msg.member.voiceState.channelID) {
		return "You are not in a voice channel";
  }

  bot.joinVoiceChannel(msg.member.voiceState.channelID).catch((err) => {
		bot.createMessage(msg.channel.id, "Error joining voice channel: " + err.message);
		console.log(err);
  }).then((connection) => {
		bot.createMessage(msg.channel.id, "Joining...");
		if (connection.playing) {
	    connection.stopPlaying();
		}

		channel_id = connection.channelID;

		connection.once("end", () => {
	    bot.createMessage(msg.channel.id, "Done");
		});
  });
}


function remove_tweets_command(msg, args) {
  
  var amt = args[0];

  if (args[0] === "all")
		amt = 10000;
  
  if (amt > 10000)
		return "Too many!";

  args = {
		screen_name: "@iambot20",
		count: amt,
		include_rts: true
  }

  T.get('statuses/user_timeline', args, function(err, data, response) {
		if (err) {
	    console.log(err);
		} else {
	    amt = data.length;
	    for (i = 0; i < amt; i++) {
				T.post('statuses/destroy/:id', { id: data[i].id_str }, on_destroy_tweet_callback)
	    }
	    if (amt != 0)
				bot.createMessage(msg.channel.id, "Removing " + amt + " tweets.");
	    else
				bot.createMessage(msg.channel.id, "No more tweets to remove!");
		}
  });
}


function tweet_command(msg, args) {
  if (args.length === 0) {
		return NO_ARGUMENT_ERROR_STRING;
  } else {
		var text = "\""
				+ args.join(" ")
				+ "\"";
		
		bot.createMessage(msg.channel.id, "Putting " + text + " on the internet forever.");

		tweet_message(text + " -" + msg.author.username);
  }
}



function tweet_message(message)
{    
  var tweet = {
		status: message
  }

  var post = T.post('statuses/update', tweet, on_tweet_callback);
}

function on_tweet_callback(err, data, response) {
  if (err) {
		console.log(err);
  } else {
		console.log("Tweeting: " + data.text);
  }

  args = {
		screen_name: "@iambot20",
		count: 1
  }
};

function on_destroy_tweet_callback(err, data, response) {
  if (err) {
		console.log(err);
  } else {
		console.log("Removing tweet: " + data.text);
  }
}

bot.connect();
